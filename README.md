# UnderCover

https://undercover.sagev.space/ is a site designed to act as a simple interface for
generating templated cover letters.

The site is built on Flask, with a very simple VanillaJS frontend, and
leverages LaTeX for document generation. User data, including logins and stored
templates, is stored in postgres, with bcrypt-hashed credentials.

## Deployment

While not implemented as part of this repo, the site itself uses a pseudo
blue/green deployment system, with Testing and Production both running on one
server. A simple script toggles the proxy between pointing visitors to the blue
or the green instance.
