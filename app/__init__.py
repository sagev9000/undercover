# Copyright Sage Vaillancourt 2021

import os
import sys

from flask import Flask, Response, make_response, render_template
from flask_minify import Minify
from werkzeug.exceptions import HTTPException

from app import routes, email


def create_app(test_config=None) -> Flask:
    app = Flask(__name__, instance_relative_config=True)
    Minify(app=app, html=True, js=True, cssless=True)

    secret_key = os.environ.get('UNDERCOVER_SECRET_KEY')
    if not secret_key:
        sys.stderr.write("WARNING: UNDERCOVER_SECRET_KEY is not defined! Application may be insecure.\n")
        secret_key = "dev"
    app.config.from_mapping(SECRET_KEY=secret_key)

    # if test_config is None:
    #     app.config.from_pyfile('config.py', silent=True)
    # else:
    #     app.config.from_mapping(test_config)

    os.makedirs(app.instance_path, exist_ok=True)

    app.register_blueprint(routes.writing_blueprint)

    @app.errorhandler(Exception)
    def internal_error(e) -> HTTPException | Response:
        if isinstance(e, HTTPException):
            return e
        email.exception_alert(e)
        return make_response(render_template('error.jinja2', status=500, error_text='Internal error occurred.', extra_text="We're looking into it!"), 500)

    @app.errorhandler(404)
    def not_found(e) -> Response:
        return make_response(render_template('error.jinja2', status=404, error_text='Page not found!'), 404)

    return app
