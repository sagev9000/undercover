import traceback
import os
import sys

from mailjet_rest import Client
from app.fallback import mock_sender

api_key = os.environ.get('MAILJET_API_KEY')
api_secret = os.environ.get('MAILJET_SECRET_KEY')

has_mailjet_keys = api_key and api_secret
mailjet = Client(auth=(api_key, api_secret), version='v3.1') if has_mailjet_keys else None

if not mailjet:
    sys.stderr.write('Mailjet keys not configured: email access is disabled.\n')
    sys.stderr.write('  Emails will be printed to the console.\n')
    sys.stderr.write('  To enable, ensure MAILJET_API_KEY and MAILJET_SECRET_KEY are set\n')
    mailjet = mock_sender


def send_password_reset(to_email: str, reset_link: str):
    data = {
        'Messages': [
            {
                "From": {
                    "Email": "donotreply@undercover.cafe",
                    "Name": "UnderCover"
                },
                "To": [{"Email": to_email}],
                "Subject": "UnderCover - Password Reset",
                "TextPart": f"Complete your UnderCover password reset by visiting {reset_link}",
                "HTMLPart": f"""
<h3>
  <a href='{reset_link}'>Click here to complete your UnderCover password reset</a>
</h3>
<br />
Or copy and paste this link into your browser: {reset_link}
""",
                "CustomID": "PasswordResetLink"
            }
        ]
    }

    result = mailjet.send.create(data=data)
    return 200 <= result.status_code <= 299


def exception_alert(e: Exception):
    exception_text = ''.join(traceback.format_exception(None, e, e.__traceback__))
    data = {
        'Messages': [
            {
                "From": {
                    "Email": "ErrorReporting@undercover.cafe",
                    "Name": "UnderCover"
                },
                "To": [{"Email": "sage@sagev.space"}],
                "Subject": "UnderCover Internal Error",
                "TextPart": exception_text,
                "HTMLPart": f"<pre>{exception_text}</pre>",
                "CustomID": "InternalServerError"
            }
        ]
    }

    mailjet.send.create(data=data)


if __name__ == "__main__":
    send_password_reset('sage@sagev.space', 'https://sagev.space/')
