import types
import json


class MockConnection:
    mock_cursor = types.SimpleNamespace()
    mock_cursor.execute = lambda *a: ()
    mock_cursor.fetchone = lambda *a: None
    mock_cursor.fetchall = lambda *a: []

    def __enter__(self, *a) -> object:
        return self

    def __exit__(self, *a) -> None:
        pass

    def cursor(self) -> object:
        return self.mock_cursor

    def commit(self, *a) -> None:
        pass


class MockCreator:
    @staticmethod
    def create(data=None, *a) -> object:
        print(json.JSONEncoder(indent=2).encode(data))
        result = types.SimpleNamespace()
        result.status_code = 200
        return result


mock_sender = types.SimpleNamespace()
mock_sender.send = MockCreator()

